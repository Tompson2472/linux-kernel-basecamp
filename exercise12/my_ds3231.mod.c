#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0x3364d393, "module_layout" },
	{ 0xc344b97a, "param_ops_charp" },
	{ 0x501e0e43, "class_destroy" },
	{ 0x5e150667, "class_remove_file_ns" },
	{ 0x6f2d808c, "i2c_del_driver" },
	{ 0x88bc573f, "class_create_file_ns" },
	{ 0x32e12e8, "__class_create" },
	{ 0xc5850110, "printk" },
	{ 0x3388efe0, "i2c_register_driver" },
	{ 0x754d539c, "strlen" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0x18615cd8, "_dev_info" },
	{ 0x87330633, "i2c_smbus_read_byte_data" },
	{ 0xbdfb6dbb, "__fentry__" },
};

MODULE_INFO(depends, "");

MODULE_ALIAS("i2c:my_ds3231");

MODULE_INFO(srcversion, "42540A20AAA52E4183D3713");
