#! /bin/bash

ACTIVE_PINS=""
COUNT=0
BUTTON=26

function gpio_init() {
    echo $1 > /sys/class/gpio/export
    echo $2 > /sys/class/gpio/gpio$1/direction
    ACTIVE_PINS+=("$1")
}

function gpio_deinit() {
    for PIN in "${ACTIVE_PINS[@]}"
    do
        echo $PIN > /sys/class/gpio/unexport
    done
}

function gpio_read() {
    GPIO_STATE=$(cat /sys/class/gpio/gpio$1/value)
}

function cont_press() {
    CURRENT_STATE=$GPIO_STATE
    gpio_read $1

    if [[ CURRENT_STATE -ne GPIO_STATE && GPIO_STATE -eq 1 ]]; then
        ((COUNT++))
    fi
}

function gpio_blink() {
    echo 1 > /sys/class/gpio/gpio$1/value
    sleep $2
    echo 0 > /sys/class/gpio/gpio$1/value
}

gpio_init $BUTTON in

while :
do 
    cont_press $BUTTON
    echo "button pressed: $COUNT"
done

gpio_deinit
