Instruction to building and installing the test system for Linux.

# Steps

1. Create shallow clone of linux-stable repository (version you need).
2. Export your build path as a variable(I'll call it 'BUILD_KERNEL').
3. Make defconfig.
4. Customize system using menuconfig.
5. Build kernel.
6. Clone buildroot sources.
7. Export your build path as a variable(I'll call it 'BUILD_ROOTFS').
8. Make defconfig and enter BUILD_ROOTFS.
9. Customize system using menuconfig. Do not choose 'Linux Kernel' option because we build it separately.
10. Create new user (by user record).
11. Add the user to sudoers.
12. Create list of shells for dropbear (add "/bin/sh" and "/bin/bash").
13. Build file system (by make).
14. Test the system using quemu.

# Commands

1. git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable -b v(your version) --depth 1
2. export BUILD_KERNEL=<your_build_path>
3. make O=${BUILD_KERNEL} i386_defconfig; cd ${BUILD_KERNEL}
4. make menuconfig
5. make
6. git clone git://git.buildroot.net/buildroot; cd buildroot
7. export BUILD_ROOTFS=<your_build_path>
8. make O=${BUILD_ROOTFS} qemu_x86_defconfig; cd ${BUILD_ROOTFS}
9. make menuconfig
10. echo "user 1000 user 1000 =pass /home/user /bin/bash - Linux User" > ${BUILD_ROOTFS}/users
11. mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d
echo "user	ALL=(ALL) ALL" > ${BUILD_ROOTFS}/root/etc/sudoers.d/user
12. mkdir -p ${BUILD_ROOTFS}/root/etc
echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells
echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells
13. make
14. qemu-system-i386 -kernel ${BUILD_KERNEL}/arch/i386/boot/bzImage -append "root=/dev/sda console=ttyS0 noapic" -drive format=raw,file=${BUILD_ROOTFS}/images/rootfs.ext3 -nic user,hostfwd=tcp::8022-:22 &
