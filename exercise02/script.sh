#! /bin/bash

if [ ! $1 ]; then
    set $(pwd)
elif [ ! -d $1 ]; then
    echo "$1 not a directory"
    exit 1
fi

DIR="$1"

FILE_LIST=$(find $DIR -maxdepth 1 -type f -mtime +30)

for index in $FILE_LIST; do
    FILE=$(basename $index)
    echo "rename $FILE to ~$FILE"
    mv $DIR/$FILE $DIR/~$FILE
done
