#!/bin/bash

if [[ ! $1 ]]; then 
    while true; do
        read -p "Enter the number:" num

        if [[ $num -gt 0 && $num -le 100 ]]; then 
            break
        else    
            echo "Number out of limits [1 .. 100].\n Try again"
        fi
    done

    set $num
fi

if [[ ! $2 ]]; then 
    while true; do
        read -p "Enter the limit[1 .. 100]:" num

        if [[ $num -gt 0 && $num -le 100 ]]; then 
            break
        else    
            echo "Number out of limits [1 .. 100]."
            echo "Try again"
        fi
    done

    set $1 $num
fi

if [[ ! $3 ]]; then 
    while true; do
        read -p "Enter number of try:" num

        if [[ $num -gt 0 ]]; then 
            break
        else    
            echo "Number out of try must be gether then 0"
            echo "Try again"
        fi
    done

    set $1 $2 $num
fi

function rnd() {
    return $(($RANDOM % $1))
} 

rnd 10
X=$?
loop=$3

while [[ loop -gt "0" ]]; do
    if [[ $1 -eq $X ]]; then 
        echo "Y is equal to X"
    elif [[ $1 -lt $X ]]; then
        echo "Y is less than X"
    else
        echo "Y is greater than X"
    fi

    let 3=3-1

    if [[ $3 -gt "0" ]]; then 
        while true; do
            read -p "Enter the number:" num

            if [[ $num -gt 0 && $num -le 100 ]]; then 
                break
            else    
                echo "Number out of limits [1 .. 100].\n Try again"
            fi
        done

        set $num
    fi
done