#! /bin/bash

SAVE_CURSORE_POSITION="\033[s"
RESTORE_CURSORE_POSITION="\033[u\033[0J"

get_file() {
    while true; do 
        read -p "Enter file to copy:" FILE;
        if [ ! f-e $FILE ]; then
            echo "$FILE : file not found. Try again"
        else
            break
        fi
    done
}

get_dir() {
    while true; do 
        read -p "Enter dirictory:" DIR;
        if [[ ! -d $DIR ]]; then 
            echo "$DIR :no such directory. Try again"
        else
            break
        fi
    done
}

menu() {
    printf $RESTORE_CURSORE_POSITION
    echo "/***************MENU***************/"
    echo "Please enter the number of command"
    echo "#1 *List directory"
    echo "#2 *Change directory"
    echo "#3 *Copy"
    echo "#4 *Move/rename"
    echo "#5 *Remove"
    echo "#6 *Exit"
    echo "/**********************************/"
    read -p "Your chose:" OPRION;
}

list() {
    printf $RESTORE_CURSORE_POSITION
    echo "/***************LIST***************/"
    printf $SAVE_CURSORE_POSITION

    ls .

    printf $SAVE_CURSORE_POSITION
}

change() {
    printf $RESTORE_CURSORE_POSITION
    echo "/*********CHANGE DIRICTORY**********/"
    
    get_dir
    cd $DIR

    printf $RESTORE_CURSORE_POSITION
    echo "/*********CURRENT DIRICTORY**********/"
    pwd
    printf $SAVE_CURSORE_POSITION
}

copy() {
    printf $RESTORE_CURSORE_POSITION
    echo "/***************COPY***************/"

    get_file
    get_dir
    cp $FILE $DIR

    printf $SAVE_CURSORE_POSITION
}

move() {
    printf $RESTORE_CURSORE_POSITION
    echo "/***************MOVE***************/"

    get_file
    get_dir
    mv $FILE $DIR

    printf $SAVE_CURSORE_POSITION
}

remove() {
    printf $RESTORE_CURSORE_POSITION
    echo "/*************REMOVE***************/"

    get_file
    rm $FILE

    printf $SAVE_CURSORE_POSITION
}

pwd
printf $SAVE_CURSORE_POSITION

while true; do
    menu

    case $OPRION in
        1) list ;;
        2) change ;;
        3) copy ;;
        4) move ;;
        5) remove ;;
        6) echo "Goodbye" ; exit 0;;
        *) ;;
    esac
done
