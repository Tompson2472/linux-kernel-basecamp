#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/slab.h>
#include <linux/ctype.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anatolii");
MODULE_DESCRIPTION("Module to convert currency");

#define NUM_STR_SIZE 20

static char* usr_num_str = "0,0";
static char* usr_factor_str = "UAH";
static int operation_num = 0;

/*-------------------base_logic_start-------------------*/
long round_to_power(long value, long power)
{
    long compare = 1;
    while(power > 0){
        compare *= 10;
        power--;
    }
    while(value >= compare)
    {
        value /= 10;
    }
    return value;
}

typedef struct float_val
{
    long primary;
    long secondary;//in range from 0 to 999
}float_val;

void float_to_str(float_val value, char* str)
{
    value.secondary = round_to_power(value.secondary, 3);
    if(value.secondary > 99)
    {
        snprintf(str, NUM_STR_SIZE, "%ld,%ld", value.primary, value.secondary);
    } else if(value.secondary > 9)
    {
        snprintf(str, NUM_STR_SIZE, "%ld,0%ld", value.primary, value.secondary);
    } else
    {
        snprintf(str, NUM_STR_SIZE, "%ld,00%ld", value.primary, value.secondary);
    }
    
}

float_val str_to_float(char* str)
{
    long primary;
    long secondary;
    float_val result;
    int is_ok1;
    int is_ok2;
    char* primary_str = kmalloc(strlen(str), GFP_KERNEL);
    char* secondary_str = kmalloc(strlen(str), GFP_KERNEL);
    int counter = 0;
    int second_counter = 0;
    char ch;
    while((ch = str[counter]) != ',')
    {
        if(ch == '\0') 
        {
            float_val null_num = {0, 0,};
            kfree(primary_str);
            return null_num;
        }
        primary_str[counter] = ch;
        counter++;
    }
    counter++;

    while((ch = str[counter]) != '\0')
    {
        secondary_str[second_counter] = ch;
        counter++;
        second_counter++;
    }
    primary = 0;
    is_ok1 = kstrtol(primary_str, 10, &primary);
    secondary = 0;
    is_ok2 = kstrtol(secondary_str, 10, &secondary);
    kfree(primary_str);
    kfree(secondary_str);

    if((is_ok1 != 0) || (is_ok2 != 0))
    {
        float_val res = {0,0,};
        return res;
    }

    result.primary = primary;
    result.secondary = secondary;

    return result;
}

float_val exchange(float_val quantity, float_val conv_factor)
{
    long primary;
    long secondary;
    float_val result;

    long primary_row = quantity.primary * conv_factor.primary;
    long primary_secondary1 = quantity.primary * conv_factor.secondary;
    long primary_secondary2 = quantity.secondary * conv_factor.primary;
    long secondary_row = quantity.secondary * conv_factor.secondary;
    secondary_row = secondary_row / 1000;

    primary = primary_row + primary_secondary1/1000 + primary_secondary2/1000;
    secondary = secondary_row + (primary_secondary1 % 1000) + (primary_secondary2 % 1000);

    if(secondary > 999)
    {
        long additional_p = 0;
        additional_p = secondary / 1000;
        primary += additional_p;
        secondary -= additional_p * 1000;
    }

    result.primary = primary;
    result.secondary = secondary;

    return result;
}
/*--------------------base_logic_end--------------------*/
static float_val eur_to_uah = {31,440};
static float_val uah_to_eur = {0,32};

/*---------------number_param_start---------------*/
int set_usr_num(const char *val, const struct kernel_param *kp)
{
    char* factor_str_without_n;
    char* usr_num_uah_str;
    float_val usr_num;
    float_val usr_num_uah;
    int i;

    int res = param_set_charp(val, kp);
    if(res == 0)
    {
        float_val conversion_factor;
        printk(KERN_INFO "User input: %s", usr_num_str);

        if(operation_num == 0)
        {
            conversion_factor = eur_to_uah;
        } else if(operation_num == 1)
        {
            conversion_factor = uah_to_eur;
        } else
        {
            printk(KERN_INFO "No such currency");
            return 0;
        }

        factor_str_without_n = kmalloc(strlen(usr_factor_str), GFP_KERNEL);//
        i = 0;
        while(i < 3)
        {
            factor_str_without_n[i] = usr_factor_str[i];
            i++;
        }
        
        usr_num = str_to_float(usr_num_str);
        usr_num_uah = exchange(usr_num, conversion_factor);
        usr_num_uah_str = kmalloc(strlen(usr_num_str+2), GFP_KERNEL);
        float_to_str(usr_num_uah, usr_num_uah_str);
        printk(KERN_INFO "In %s:(%s)\n", factor_str_without_n, usr_num_uah_str);
        kfree(usr_num_uah_str);
        kfree(factor_str_without_n);
        
        return 0;
    }
    return -1;
}

int get_usr_num(char *buffer, const struct kernel_param *kp)
{
    int res = param_get_charp(buffer, kp);
    if(res == 0)
    {
        return 0;
    }
    return -1;
}

const struct kernel_param_ops usr_num_ops =
{
    .set = &set_usr_num,
    .get = &get_usr_num,
};

module_param_cb(usr_num_str, &usr_num_ops, &usr_num_str, 0644);
MODULE_PARM_DESC(usr_num_str, "User money quantity");
/*----------------number_param_end----------------*/

/*---------------factor_param_start---------------*/
int set_usr_factor(const char *val, const struct kernel_param *kp)
{
    int res = param_set_charp(val, kp);
    if(res == 0)
    {
        if(strcmp(usr_factor_str, "UAH\n") == 0)
        {
            operation_num = 0;
        } else if(strcmp(usr_factor_str, "EUR\n") == 0)
        {
            operation_num = 1;
        } else
        {
            printk(KERN_INFO "No such currency");
            return 0;
        }
        
        return 0;
    }
    return -1;
}

int get_usr_factor(char *buffer, const struct kernel_param *kp)
{
    int res = param_get_charp(buffer, kp);
    if(res == 0)
    {
        return 0;
    }
    return -1;
}

const struct kernel_param_ops usr_factor_ops =
{
    .set = &set_usr_factor,
    .get = &get_usr_factor,
};

module_param_cb(usr_factor_str, &usr_factor_ops, &usr_factor_str, 0644);
MODULE_PARM_DESC(usr_factor_str, "Currency conversion factor");
/*----------------factor_param_end----------------*/

static int __init conv_init(void)
{
    printk(KERN_INFO "Converter module is loaded");
    return 0;
}

static void __exit conv_exit(void)
{
    printk(KERN_INFO "Converter module is unloaded");
}

module_init(conv_init);
module_exit(conv_exit);
