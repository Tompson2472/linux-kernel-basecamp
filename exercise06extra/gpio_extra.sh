#! /bin/bash

COUNT=0
RED=16
GREEN=20
BLUE=21
BUTTON=26

function gpio_init() {
    echo $1 > /sys/class/gpio/export
    echo $2 > /sys/class/gpio/gpio$1/direction
    ACTIVE_PINS+=("$1")
}

function gpio_deinit() {
    for PIN in "${ACTIVE_PINS[@]}"
    do
        echo $PIN > /sys/class/gpio/unexport
    done
}

function gpio_read() {
    GPIO_STATE=$(cat /sys/class/gpio/gpio$1/value)
}

function cont_press() {
    CURRENT_STATE=$GPIO_STATE
    gpio_read $1

    if [[ $CURRENT_STATE -ne $GPIO_STATE && $GPIO_STATE -eq 1 ]]; then
        while [ $GPIO_STATE -eq 1 ]
        do
            gpio_read $1
        done

        ((COUNT++))
        echo "count: $COUNT"
    fi
}

function gpio_blink() {
    echo 1 > /sys/class/gpio/gpio$1/value
    sleep $2
    echo 0 > /sys/class/gpio/gpio$1/value
}

gpio_init $RED out 
gpio_init $GREEN out 
gpio_init $BLUE out 
gpio_init $BUTTON in    

echo "waiting button press"

while [ $COUNT -eq 0 ]
do
    cont_press $BUTTON
done

while :
do 
    cont_press $BUTTON

    if [ $(($COUNT % 2)) -eq 1 ]; then
        echo "MOD 1"
        gpio_blink $RED
        gpio_blink $GREEN
        gpio_blink $BLUE
    elif [ $COUNT -lt 10 ]; then
        echo "MOD 2"
        gpio_blink $BLUE
        gpio_blink $GREEN
        gpio_blink $RED
    else
        break;
    fi    
done

gpio_deinit
