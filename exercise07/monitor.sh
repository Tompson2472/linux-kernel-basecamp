#!/bin/bash

function print_temp {
	whole_t=$(i2cget -y 1 0x68 0x11)
	whole_t=$(echo $whole_t | cut -d'x' -f 2)
	whole_t=$((16#$whole_t))
	fract_t=$(i2cget -y 1 0x68 0x12)
	fract_t=$(echo $fract_t | cut -d'x' -f 2)
	fract_t=$((16#$fract_t))
	fract_t=$(($fract_t*100/256))
	echo -n $whole_t
	echo -n ","
	echo $fract_t
}

function get_sec {
	sec=$(i2cget -y 1 0x68 0x00)
	sec=$(echo $sec | cut -d'x' -f 2)
}

function print_time {
	hour=$(i2cget -y 1 0x68 0x02)
	hour=$(echo $hour | cut -d'x' -f 2)
	hour=$(($hour + 3))
	if [ $hour -gt 23 ]; then hour=$(($hour - 24)); fi
	min=$(i2cget -y 1 0x68 0x01)
	min=$(echo $min | cut -d'x' -f 2)
	get_sec
	clock_time=$hour":"$min":"$sec
	echo $clock_time
}

but_val=0
buf=0
is_pushed=0

echo 26 > /sys/class/gpio/unexport
echo 26 > /sys/class/gpio/export

while true
do
buf=$(cat /sys/class/gpio/gpio26/value)
if [ $buf -ne $but_val ]; then
	if [ $is_pushed -eq 1 ]; then
		is_pushed=0
		get_sec
		sec_dif=$(($sec-$cur_sec))
		if [ $sec_dif -lt 2 ]; then
			print_temp
			print_time
		else
			break
		fi
	fi
	but_val=$buf
	if [ $buf -eq 1 ]; then
		is_pushed=1
		get_sec
		cur_sec=$sec
	fi
fi
done
