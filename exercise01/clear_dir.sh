#! /bin/bash
TEST=""
FLAGS=""
REQURSION="-maxdepth 1"

usage()
{
  echo "Usage: clear_dir [ -h | --help ] [ -r | --recursive ]
                         [ -y | --yes ] [ -t | --test ] dirictory"
}

PARSED_ARGUMENTS=$(getopt -n clear_dir -o hryt --long help,recursive,yes,test -- "$@")
if [[ $? -ne "0" || $# -gt "2" ]]; then
  echo $?
  usage
  exit 1
fi

# echo "PARSED_ARGUMENTS is $PARSED_ARGUMENTS"
eval set -- "$PARSED_ARGUMENTS"

while :
do
	case "$1" in
		-h | --help)      usage ; exit 0 ;;
		-r | --recursive) REQURSION="" ; shift ;;
		-y | --yes)       FLAGS="-f " ; shift ;;
		-t | --test)      TEST=1 ; shift ;;
		--) shift; break ;;
		*) echo "Unexpected option: $1 - this should not happen."
		usage ;;
	esac
done

if [[ $# -eq 0 ]]; then
	set -- "."
elif [[ ! -d $1 ]]; then
	usage
	exit 1
fi

if [[ $TEST -eq "1" ]]; then 
	echo $(find $1 -type f $REQURSION \( -name "*.txt" -o -name "~*" -o -name "-*" -o -name "_*" \))
else 
	find $1 -type f $REQURSION \( -name "*.txt" -o -name "~*" -o -name "-*" -o -name "_*" \) -exec rm $FLAGS{} \;
fi
